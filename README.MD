# A tiny webserver symlink manager

## What is this for?

There's a pattern for managing configuration for web servers (at least `NGINX`),
where the actual configuration files live in a folder that is not directly read
by the web server itself. Instead, when these are supposed to be "active", then
a symlink is created in a different location. This enables the configuration
files to be easily switched "on and off". But it can still be a bit of a hassle
making sure everything is nice and clean. This script aims to help manage these
symlinks and decrease the chance for mismatching jank, typos and whatnot...

### "That was too wordy, explain it to me like I'm a nerd!"


All right... given an `NGINX` conf file that at one point reads something like:

```
    include /usr/local/etc/nginx/sites-enabled/*;
```

And where that folder looks a bit like this:

```
ls -l /usr/local/etc/nginx/sites-enabled

site1.example.net -> /usr/local/etc/nginx/sites-available/site1.example.net
site2.example.net -> /usr/local/etc/nginx/sites-available/site2.example.net
service1.example.net -> /usr/local/etc/nginx/sites-available/service1.example.net
service2.example.net -> /usr/local/etc/nginx/sites-available/service2.example.net
```

This script helps manage the creation and deletion of these symbolic links, as well
as doing some verification of whether they are all correct and consistent.

## Does this work with webservers other than NGINX?

I assume so, as long as that pattern makes sense in their conf. I'm not super deep
in web and it's been quite a while since I've used anything else. The gist of
the functionality here is creating and destroying symlinks as well as making sure
things point to the expected places, etc...

## How to use this script

This is a simple Bash script. You can place it in the place of most convenience
and then edit it to alter the variables at the top, pointing the script to the
places you want. The important variables here, that get used throughout are
`SITES_AVAILABLE_DIR` and `SITES_ENABLED_DIR`. The script expects the following:

- `SITES_AVAILABLE_DIR` contains regular files (not links, not dirs)
- `SITES_ENABLED_DIR` contains symbolic links to those conf files
    - The names of these links and their targets are exact matches
    - The linked paths are all absolute
    - All links point to files in `SITES_AVAILABLE_DIR`

Situations other than these are considered configuration errors. The script can
fix some of those, such as broken links and relative paths instead of absolutes,
but will not take actions in situations where there isn't a clear answer (such
as when there are duplicate files).

For more information, feel free to run `websites.sh -h` to get a list of available
options.

### Testing

I've also bundled together a small companion script, `create-test-files.sh`. As
the name implies, this script creates and populates a small tree of files with
several different situations. Do have a play around with this script to find out
how the script respond to different situations (what it'll catch, what it'll fix...).


## Warnings and caveats

This script will never remove anything from `SITES_AVAILABLE_DIR` it will also
not touch anything outside of that directory and `STIES_ENABLED_DIR` EXCEPT when
you ask it to move a misplaced file.

Please keep in mind that while I do try to double check things thoroughly before
taking any destructive action, the script never really asks for explicit confirmation
before doing those. It won't "automatically" do something, but if you ask it to
"fix" a site and it's one of the conditions it has an answer for, it'll do it.

As usual, please be extra careful about using this in any sort of production
environment. This is NOT any sort of professional tool. Make sure your stuff
is always backed up, etc... If your site list is all `ON` and `OFF`, everything
should be smooth but do be cautious if you're cleaning up preexisting messy
folders.


## Contributing and feedback

I wrote this script really to help me manage my own server. As such, I'm unlikely
to implement anything much more fancy than this, it's already way overscoped as is.
But feel free to drop in suggestions or Merge Requests if you feel like it. You're
also welcome to fork this up and turn its guts inside out, if you want.

If it turns out there IS some situation which I missed, especially one where the
script could have unintended behaviour, then I'm definitely interested in knowing
about that.
