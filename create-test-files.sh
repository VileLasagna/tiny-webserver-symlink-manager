#!/bin/bash

# This script sets up some test files to verify the functionality of the adjacent
# websites.sh
# It will not clean up after itself to prevent it deleting actual user data.
# It will also not do any actual testing of the script itself, just set it up so
# that you, as a user, can have a feel for what it does and how it actually behaves

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )


if [[ -e ${SCRIPT_DIR}/sites-available-test-dir ]] || [[ -e ${SCRIPT_DIR}/sites-enabled-test-dir ]] || [[ -e ${SCRIPT_DIR}/out-of-scope-test-dir ]]; then
    echo "It seems like one or more files/driectories already exist"
    echo "This script creates and populates the following folders"
    echo ""
    echo "sites-available-test-dir -> for actual conf file targets"
    echo "sites-enabled-test-dir   -> for the links which control availability"
    echo "out-of-scope-test-dir    -> for files which shouldn't be referenced"
    echo "please make sure these directories do not exist and run the script again"
    exit 1;
fi

mkdir ${SCRIPT_DIR}/sites-available-test-dir ${SCRIPT_DIR}/sites-enabled-test-dir ${SCRIPT_DIR}/out-of-scope-test-dir

touch ${SCRIPT_DIR}/sites-available-test-dir/on.example.com
touch ${SCRIPT_DIR}/sites-available-test-dir/off.example.com
touch ${SCRIPT_DIR}/sites-available-test-dir/relative.example.com
touch ${SCRIPT_DIR}/sites-available-test-dir/mismatch.example.com
touch ${SCRIPT_DIR}/sites-available-test-dir/duplicate.example.com


touch ${SCRIPT_DIR}/out-of-scope-test-dir/duplicate.example.com
touch ${SCRIPT_DIR}/out-of-scope-test-dir/misplaced.example.com
touch ${SCRIPT_DIR}/out-of-scope-test-dir/not.raw.example.com

touch ${SCRIPT_DIR}/sites-enabled-test-dir/not.link.example.com

ln -s ${SCRIPT_DIR}/out-of-scope-test-dir/not.raw.example.com ${SCRIPT_DIR}/sites-available-test-dir/not.raw.example.com
ln -s ${SCRIPT_DIR}/out-of-scope-test-dir/non.existent.example.com ${SCRIPT_DIR}/sites-available-test-dir/not.raw.broken.example.com

ln -s ${SCRIPT_DIR}/sites-available-test-dir/on.example.com ${SCRIPT_DIR}/sites-enabled-test-dir/on.example.com
ln -s ../sites-available-test-dir/relative.example.com ${SCRIPT_DIR}/sites-enabled-test-dir/relative.example.com
ln -s ${SCRIPT_DIR}/sites-available-test-dir/mismatch.example.com ${SCRIPT_DIR}/sites-enabled-test-dir/mismatched.example.com
ln -s ${SCRIPT_DIR}/sites-available-test-dir/broken.example.com ${SCRIPT_DIR}/sites-enabled-test-dir/broken.example.com
ln -s ${SCRIPT_DIR}/out-of-scope-test-dir/duplicate.example.com ${SCRIPT_DIR}/sites-enabled-test-dir/duplicate.example.com
ln -s ${SCRIPT_DIR}/out-of-scope-test-dir/misplaced.example.com ${SCRIPT_DIR}/sites-enabled-test-dir/misplaced.example.com

echo "All done. Give websites.sh -l a try and see what a mess we have"
