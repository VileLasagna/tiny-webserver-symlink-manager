#!/bin/bash


# Return error codes
# 0 - Normal exit
# 1 - invalid parameter
# 2 - Website is not in SITES_AVAILABLE_DIR when it was expected
# 3 - Website is NOT LINK when it wasn't expected
# 4 - Error creating or removing file/link
# 5 - Script refused to manage files outside of SITES_ENABLED_DIR or SITES_AVAILABLE_DIR
# 6 - Unexpected error
# 7 - Directores not found. Script misconfigured?


NGINX_DIR=/usr/local/etc/nginx
#SITES_AVAILABLE_DIR=${NGINX_DIR}/sites-available
#SITES_ENABLED_DIR=${NGINX_DIR}/sites-enabled

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

EDITOR="nano"
NGINX_DIR="${SCRIPT_DIR}"
SITES_AVAILABLE_DIR="${NGINX_DIR}/sites-available-test-dir"
SITES_ENABLED_DIR="${NGINX_DIR}/sites-enabled-test-dir"

BLACK="\033[30m"
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[34m"
PINK="\033[35m"
CYAN="\033[36m"
WHITE="\033[37m"
NORMAL="\033[0;39m"

display_help() {
    echo -e $WHITE"Script for managing websites on an nginx server"
    echo -e $NORMAL" This tool only manages linking and editing configuration files"
    echo " it DOES NOT actually look into or configure nginx itself"
    echo " it presumes configuration files for websites in the SITES_AVAILABLE_DIR"
    echo " which are enabled or disabled by symlinks to them in SITES_ENABLED_DIR"
    echo " The tool assumes/requires that both original files and links have the"
    echo " same file name. It also presumes all links have full paths. Relative"
    echo " paths are treated as an error that the tool is able to automatically"
    echo " correct"
    echo ""
    echo -e $WHITE"Usage: "$NORMAL"$(basename "$0") [OPTION] [SITENAME]"
    echo ""
    echo -e $WHITE"Where:"$NORMAL
    echo "    -h    Show this help text"
    echo -e "    -l    List sites available and their status:"
    echo -e "              ["$GREEN"ON"$NORMAL"]         configuration file and symlink exist"
    echo -e "              ["$YELLOW"OFF"$NORMAL"]        configuration file exists but symlink doesn't"
    echo -e "              ["$RED"BROKEN"$NORMAL"]     symlink exists but configuration file doesn't"
    echo -e "              ["$CYAN"MISPLACED"$NORMAL"]  symlink exists and is valid but points to a file NOT in SITES_AVAILABLE_DIR"
    echo -e "              ["$CYAN"MISMATCH"$NORMAL"]   symlink's name and target name differ. This is not expected"
    echo -e "              ["$BLUE"RELATIVE"$NORMAL"]   symlink is relative isntead of absolute"
    echo -e "              ["$PINK"DUPLICATE"$NORMAL"]  both symlink and matching configuration exist but link points to the wrong path"
    echo -e "              ["$PINK"NOT LINK"$NORMAL"]   raw file exists in SITES_ENABLED_DIR instead of a symlink"
    echo -e "              ["$PINK"NOT RAW"$NORMAL"]    symlink file exists in SITES_AVAILABLE_DIR instead of a raw conf"
    echo "    -c    Launch a text editor( ${EDITOR} ) to edit a site's configuration"
    echo "    -e    Enables a site by creating a symlink to it in the sites-enabled dir"
    echo "    -d    disables a site by removing the symlink to it from the sites-enabled dir"
    echo -e "    -f    fixes ["$RED"BROKEN"$NORMAL"], ["$PINK"DUPLICATE"$NORMAL"],  ["$BLUE"RELATIVE"$NORMAL"] or ["$PINK"NOT LINK"$NORMAL"] websites"
    echo -e "              ["$RED"BROKEN"$NORMAL"]     symlink will be removed"
    echo -e "              ["$PINK"DUPLICATE"$NORMAL"]  symlink will be removed and created pointing to full path on SITES_AVAILABLE_DIR"
    echo -e "              ["$BLUE"RELATIVE"$NORMAL"]   symlink will be removed and created pointing to full path on SITES_AVAILABLE_DIR"
    echo -e "              ["$PINK"NOT LINK"$NORMAL"]   raw file will be moved and symlinked"
}

is_path_in_scope() {
    # Tests whether a path is contained within SITES_AVAILABLE_DIR
    PATH_DIR="$(dirname $1)"2> /dev/null
    # echo "ARG -> $1"
    # echo "PATH_DIR -> ${PATH_DIR}"

    if [[ "${PATH_DIR}" =~ ^/ ]]; then
    # Path is absolute, easy check
        if [[ "${PATH_DIR}" =~ ^$(realpath "${SITES_AVAILABLE_DIR}") ]]; then
            return 1;
        else
            return 0;
        fi
    else
        if [[ $(realpath -L "${SITES_ENABLED_DIR}/${PATH_DIR}") =~ ^"${SITES_AVAILABLE_DIR}" ]]; then
            return 1;
        else
            return 0;
        fi
    fi
}

site_status() {

    #   Return codes are:
    # 0 - ON
    # 1 - OFF
    # 2 - Relative path link instead of absolute
    # 3 - Link points to a valid file that is NOT the actual conf file which ALSO exists
    # 4 - File in link folder is NOT A LINK
    # 5 - Link points to a valid file that is not in the actual conf
    # 6 - Link is broken
    # 7 - Link's target has mismatched filename
    # 8 - File in conf folder is a link

    [ -e "${SITES_AVAILABLE_DIR}/${SITE}" ]
    CONF_EXISTS=$?
    if [ -e "${SITES_ENABLED_DIR}/${SITE}" ] || [ -L "${SITES_ENABLED_DIR}/${SITE}" ]; then
        LINK_EXISTS=0
    else
        LINK_EXISTS=1
    fi
    [ -L "${SITES_ENABLED_DIR}/${SITE}" ]
    LINK_IS_LINK=$?

    [ -L "${SITES_AVAILABLE_DIR}/${SITE}" ]
    CONF_IS_LINK=$?

    if (( LINK_EXISTS == 0 )) && (( LINK_IS_LINK == 0 )); then
        is_path_in_scope $(readlink -- "${SITES_ENABLED_DIR}/${SITE}")
        TARGET_IS_IN_SCOPE=$?

        if [[ "$(readlink -- "${SITES_ENABLED_DIR}/${SITE}")" =~ ^/ ]]; then
            TARGET_IS_ABSOLUTE=0
            [ -f $(readlink -- "${SITES_ENABLED_DIR}/${SITE}") ]
            LINK_IS_VALID=$?

            if [[ $(realpath $(readlink -- "${SITES_ENABLED_DIR}/${SITE}")) == "${SITES_AVAILABLE_DIR}/${SITE}" ]]; then
                TARGET_IS_CONF=0
            else
                TARGET_IS_CONF=1
            fi
        else
            TARGET_IS_ABSOLUTE=1
            [ -f ${SITES_ENABLED_DIR}/$(readlink -- "${SITES_ENABLED_DIR}/${SITE}") ]
            LINK_IS_VALID=$?
            if [ -e "${SITES_ENABLED_DIR}/${SITE}" ]; then
                if [[ $(realpath ${SITES_ENABLED_DIR}/$(readlink -- "${SITES_ENABLED_DIR}/${SITE}") ) == "${SITES_AVAILABLE_DIR}/${SITE}" ]]; then
                    TARGET_IS_CONF=0
                else
                    TARGET_IS_CONF=1
                fi
            else
                TARGET_IS_CONF=1
            fi
        fi

    else
        LINK_IS_VALID=1
        TARGET_IS_IN_SCOPE=1
        TARGET_IS_CONF=1
    fi

    if (( LINK_EXISTS == 0 )) && (( LINK_IS_LINK != 0 )); then
        echo 4;
        return 0;
    fi

    if (( CONF_IS_LINK == 0 )); then
        echo 8;
        return 0;
    fi

    if (( CONF_EXISTS == 0 )) && (( LINK_EXISTS != 0 )); then
        echo 1;
        return 0;
    fi

    if (( LINK_EXISTS == 0 )) && (( LINK_IS_VALID != 0 )); then
        echo 6;
        return 0;
    fi

    if (( LINK_EXISTS == 0 )) && (( LINK_IS_LINK == 0 )) && (( LINK_IS_VALID == 0 )); then
        if [[ $(basename "${SITES_ENABLED_DIR}/${SITE}") != $(basename $(readlink -- "${SITES_ENABLED_DIR}/${SITE}")) ]]; then
            echo 7;
            return 0;
        else
            if (( TARGET_IS_CONF != 0 )); then
                if (( CONF_EXISTS == 0 )) && (( TARGET_IS_IN_SCOPE == 0 )); then
                    echo 3;
                    return 0;
                else
                    echo 5;
                    return 0;
                fi
            elif (( CONF_EXISTS == 0 )); then
                if (( TARGET_IS_ABSOLUTE == 0 )); then
                    echo 0;
                    return 0;
                else
                    echo 2;
                    return 0;
                fi
            else
                # This should be unreachable. The link is valid AND points to the conf file but that doesn't exist?
                # Something went wrong somewhere
                echo "Error: Unexpected conditions in site checking"
                echo "SITE               -> ${SITE}"
                echo "CONF_EXISTS        -> ${CONF_EXISTS}"
                echo "LINK_EXISTS        -> ${LINK_EXISTS}"
                echo "LINK_IS_LINK       -> ${LINK_IS_LINK}"
                echo "LINK_IS_VALID      -> ${LINK_IS_VALID}"
                echo "TARGET_IS_IN_SCOPE -> ${TARGET_IS_IN_SCOPE}"
                echo "TARGET_IS_CONF     -> ${TARGET_IS_CONF}"
                return 6;
            fi
        fi
    fi

    echo "Error: Condition not captured in site checking"
    echo "SITE               -> ${SITE}"
    echo "CONF_EXISTS        -> ${CONF_EXISTS}"
    echo "LINK_EXISTS        -> ${LINK_EXISTS}"
    echo "LINK_IS_LINK       -> ${LINK_IS_LINK}"
    echo "LINK_IS_VALID      -> ${LINK_IS_VALID}"
    echo "TARGET_IS_IN_SCOPE -> ${TARGET_IS_IN_SCOPE}"
    echo "TARGET_IS_CONF     -> ${TARGET_IS_CONF}"
    return 6;
}

print_status() {
    SITE=$1
    STATUS_CODE=$(site_status ${SITE})
    case $STATUS_CODE in
        0)
            echo -e "["$GREEN"ON"$NORMAL$"]         ${SITE}";
            ;;
        1)
            echo -e "["$YELLOW"OFF"$NORMAL$"]        ${SITE}";
            ;;
        2)
            echo -e "["$BLUE"RELATIVE"$NORMAL"]   ${SITE}";
            ;;
        3)
            echo -e "["$PINK"DUPLICATE"$NORMAL"]  ${SITE}";
            ;;
        4)
            echo -e "["$PINK"NOT LINK"$NORMAL$"]   ${SITE}";
            ;;
        5)
            echo -e "["$CYAN"MISPLACED"$NORMAL"]  ${SITE}";
            ;;
        6)
            echo -e "["$RED"BROKEN"$NORMAL$"]     ${SITE}";
            ;;
        7)
            echo -e "["$CYAN"MISMATCH"$NORMAL$"]   ${SITE}";
            ;;
        8)
            echo -e "["$PINK"NOT RAW"$NORMAL$"]    ${SITE}";
            ;;
        *)
            echo "Unexpected return from site checking function"
            echo "RETURN CODE        -> $STATUS_CODE"
            echo "SITE               -> ${SITE}"
            echo "CONF_EXISTS        -> ${CONF_EXISTS}"
            echo "LINK_EXISTS        -> ${LINK_EXISTS}"
            echo "LINK_IS_LINK       -> ${LINK_IS_LINK}"
            echo "LINK_IS_VALID      -> ${LINK_IS_VALID}"
            echo "TARGET_IS_IN_SCOPE -> ${TARGET_IS_IN_SCOPE}"
            echo "TARGET_IS_CONF     -> ${TARGET_IS_CONF}"
            exit 6;
            ;;
    esac;
}


 #&& [ $(find ${SITES_AVAILABLE_DIR} -name ${SITE})]
list_sites() {

    SITES_AVAILABLE_LIST=$( ls -w1 ${SITES_AVAILABLE_DIR} )
    SITES_ENABLED_LIST=$( ls -w1 ${SITES_ENABLED_DIR} )
    SITES_LIST="${SITES_AVAILABLE_LIST[@]}"
    SITES_LIST+=$'\n'
    SITES_LIST+="${SITES_ENABLED_LIST[@]}"

    SITES_LIST=$(echo "${SITES_LIST}" | sort -u)
    echo "Sites available are:"
    echo ""

    for SITE in ${SITES_LIST}; do
        print_status $SITE
    done;

}

configure_site() {
    $EDITOR ${SITES_AVAILABLE_DIR}/$1
}

enable_site() {

    if [[ ! -e ${SITES_AVAILABLE_DIR}/$1 ]]; then
        echo -e "can't find conf file for website "$CYAN"$1"$NORMAL". Please double check argument"
        exit 1;
    fi

    if [ -L "${SITES_ENABLED_DIR}/$1" ]; then
        if   [ !  -e "${SITES_AVAILABLE_DIR}/$1" ]; then
            echo -e $RED"ERROR:"$NORMAL" Website "$CYAN"$1"$NORMAL" is ENABLED but not AVAILABLE";
            exit 2;
        else
            echo -e "Website "$CYAN"$1"$NORMAL" is already "$GREEN"ENABLED"$NORMAL;
        fi
    else
        if [ !  -e "${SITES_AVAILABLE_DIR}/$1" ]; then
            echo -e "Website "$CYAN"$1"$NORMAL" is not available. Try -l?"
            exit 2;
        else
            ln -s ${SITES_AVAILABLE_DIR}/$1 ${SITES_ENABLED_DIR}/$1
            if [ -L "${SITES_ENABLED_DIR}/$1" ]; then
                    echo -e "Website "$CYAN"$1"$NORMAL" is now "$GREEN"ENABLED"$WHITE;
            else
                    echo -e "Error enabling website "$CYAN"$1"$NORMAL", it is "$YELLOW"DISABLED"$NORMAL;
                    exit 4;
            fi
        fi
    fi
}

disable_site() {
    if [  ! -L "${SITES_ENABLED_DIR}/$1" ]; then
        if [ -e "${SITES_ENABLED_DIR}/$1" ]; then
            echo -e $RED"ERROR:"$NORMAL" File for website "$CYAN"$1"$NORMAL" in the sites available directory is "$PINK"NOT A SYMLINK"$NORMAL". Try -f first";
            exit 3;
        else
            echo -e "Website "$CYAN"$1"$NORMAL" is already "$YELLOW"DISABLED"$NORMAL;
        fi
    else
        if [ !  -e "${SITES_AVAILABLE_DIR}/$1" ]; then
            echo -e "Website "$CYAN"$1"$NORMAL" is not available. Try -l?"
            exit 2;
        else
            rm ${SITES_ENABLED_DIR}/$1

            if [ ! -L "${SITES_ENABLED_DIR}/$1" ]; then
                    echo -e "Website "$CYAN"$1"$NORMAL" is now "$YELLOW"DISABLED"$WHITE;
            else
                    echo -e "Error disabling website "$CYAN"$1"$NORMAL", it is "$GREEN"ENABLED"$NORMAL;
                    exit 4;
            fi
        fi
    fi
}

fix_site() {

    SITE=$1
    STATUS_CODE=$(site_status $SITE)

    if [[ ! -e ${SITES_ENABLED_DIR}/${SITE} ]] && [[ ! -L ${SITES_ENABLED_DIR}/${SITE} ]] && [[ ! -e ${SITES_AVAILABLE_DIR}/${SITE} ]] && [[ ! -L ${SITES_AVAILABLE_DIR}/${SITE} ]]; then
        echo -e "can't find website "$CYAN"${SITE}"$NORMAL". Please double check argument"
        exit 1;
    fi

    case $STATUS_CODE in
        0)
            echo -e "Nothing to fix";
            ;;
        1)
            echo -e "Nothing to fix";
            ;;
        2)
            echo -e "Link file for website "$CYAN"$SITE"$NORMAL" is relative instead of absolute. Remaking link";
            echo "OLD LINK = $(readlink -- ${SITES_ENABLED_DIR}/${SITE})"
            rm ${SITES_ENABLED_DIR}/${SITE}
            ln -s ${SITES_AVAILABLE_DIR}/${SITE} ${SITES_ENABLED_DIR}/${SITE}
            echo "Done. Site status:"
            print_status ${SITE}
            ;;
        3)
            echo -e "Link file for website "$CYAN"$SITE"$NORMAL" points to an external file. Remaking link";
            echo "WARNING: The file pointed to DOES exist and will not be touched";
            echo "OLD LINK TARGET = $(readlink -- ${SITES_ENABLED_DIR}/${SITE})"
            rm ${SITES_ENABLED_DIR}/${SITE}
            enable_site $1;
            echo "Done. Site status:"
            print_status ${SITE}
            ;;
        4)
            echo "Link file is actually a regular file"
            echo -e "Moving configuration file for website "$CYAN"$SITE"$NORMAL" to the correct location"
            mv -n "${SITES_ENABLED_DIR}/$1" "${SITES_AVAILABLE_DIR}/$1"
            if [ -e "${SITES_AVAILABLE_DIR}/${SITE}" ] && [ ! -e "${SITES_ENABLED_DIR}/${SITE}" ]; then
                echo "Success. Enabling website"
                enable_site $1;
            else
                echo -e "Error moving configuration file"
                exit 4;
            fi


            rm ${SITES_ENABLED_DIR}/${SITE}
            ln -s ${SITES_AVAILABLE_DIR}/${SITE} ${SITES_ENABLED_DIR}/${SITE}
            echo "Done. Site status:"
            print_status ${SITE}
            ;;
        5)
            echo -e "Conf file for website "$CYAN"$SITE"$NORMAL" seems to be misplaced";
            echo "File will be moved and link will be fixed"
            echo "OLD TARGET = $(readlink -- ${SITES_ENABLED_DIR}/${SITE})"
            echo ""
            mv -n "$(readlink -- ${SITES_ENABLED_DIR}/${SITE})" "${SITES_AVAILABLE_DIR}/${SITE}"

            if [ -e "${SITES_AVAILABLE_DIR}/${SITE}" ]; then
                echo "Success. Removing old link"
                rm "${SITES_ENABLED_DIR}/${SITE}"
                if [ ! -e "${SITES_ENABLED_DIR}/${SITE}" ]; then
                    echo "Success. Enabling website"
                    enable_site $1;
                fi
            else
                echo -e "Error moving configuration file"
                exit 4;
            fi

            rm ${SITES_ENABLED_DIR}/${SITE}
            ln -s ${SITES_AVAILABLE_DIR}/${SITE} ${SITES_ENABLED_DIR}/${SITE}
            echo "Done. Site status:"
            print_status ${SITE}
            ;;
        6)
            echo -e "Link file for website "$CYAN"$SITE"$NORMAL" is broken";
            echo "Link will be removed"
            echo "OLD TARGET (Not found) = $(readlink -- ${SITES_ENABLED_DIR}/${SITE})"
            echo ""
            rm "${SITES_ENABLED_DIR}/${SITE}"
            if [ ! -e "${SITES_ENABLED_DIR}/${SITE}" ] && [ ! -L "${SITES_ENABLED_DIR}/${SITE}" ]; then
                echo "Broken link removed succesfully"
            fi
            ;;
        7)
            echo -e "Link file for website "$CYAN"$SITE"$NORMAL" points is mismatched";
            echo "It points to a conf with a different filename"
            if [ -e "$(readlink -- ${SITES_ENABLED_DIR}/${SITE})" ]; then
                echo "The target DOES exist"
                echo "Mismatched link   -> ${SITES_ENABLED_DIR}/${SITE})"
                echo "Mismatched target -> $(readlink -- ${SITES_ENABLED_DIR}/${SITE})"
                echo ""
                echo "No action was taken. Please review manually"
            else
                echo "The target does NOT exist, meaning it is a broken link anyway"
                echo "Link will be cleaned up"
                echo "Mismatched link   -> ${SITES_ENABLED_DIR}/${SITE})"
                echo "Mismatched target -> $(readlink -- ${SITES_ENABLED_DIR}/${SITE})"
                rm ${SITES_ENABLED_DIR}/${SITE}
                if [ ! -e "${SITES_ENABLED_DIR}/${SITE}" ] && [ ! -L "${SITES_ENABLED_DIR}/${SITE}" ]; then
                    echo "Broken link removed succesfully"
                fi
            fi
            ;;
        8)
            echo -e "Conf file for website "$CYAN"$SITE"$NORMAL" is a symlink";
            echo "While this MAY not be an error, it IS unexpected.";
            echo "File will not be touched, please resolve manually"
            echo "Unexpected link -> ${SITES_AVAILABLE_DIR}/${SITE}"
            echo "Unexpected target -> $(readlink -- ${SITES_AVAILABLE_DIR}/${SITE})"
            if [[ -e $(readlink -- ${SITES_AVAILABLE_DIR}/${SITE}) ]]; then
                echo "The link DOES seem to resolve to an actual file:"
            else
                echo "The link appears to be broken"
            fi
            ;;
        *)
            echo "Unexpected return from site checking function"
            echo "RETURN CODE        -> $STATUS_CODE"
            echo "SITE               -> ${SITE}"
            echo "CONF_EXISTS        -> ${CONF_EXISTS}"
            echo "LINK_EXISTS        -> ${LINK_EXISTS}"
            echo "LINK_IS_LINK       -> ${LINK_IS_LINK}"
            echo "LINK_IS_VALID      -> ${LINK_IS_VALID}"
            echo "TARGET_IS_IN_SCOPE -> ${TARGET_IS_IN_SCOPE}"
            echo "TARGET_IS_CONF     -> ${TARGET_IS_CONF}"
            exit 6;
            ;;
    esac;
}

# Reset prompt colour
echo -ne $NORMAL

if [[ ! -d ${SITES_ENABLED_DIR} ]] || [[ ! -d ${SITES_AVAILABLE_DIR} ]]; then
    echo "Can't find the folders for the files. Double check both those and the script's configuration"
    echo "Expected SITES_ENABLED_DIR -> ${SITES_ENABLED_DIR}"
    echo "Expected SITES_AVAILABLE_DIR -> ${SITES_AVAILABLE_DIR}"
    exit 7;
fi

while getopts "lc:e:d:f:t:h" opt; do
  case "$opt" in
    h)
        display_help
        exit 0
        ;;
    l)
        list_sites
        exit 0
        ;;
    c)
        configure_site ${OPTARG}
        exit 0
        ;;
    e)
        enable_site ${OPTARG}
        exit 0
        ;;
    d)
        disable_site ${OPTARG}
        exit 0
        ;;
    f)
        fix_site ${OPTARG}
        exit 0
        ;;
    \?)
       echo "Invalid parameter"
       echo "$0 -h for more help"
       exit 1
       ;;
  esac
done

# Display help if no option selected
display_help
exit 0
